<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/home.css">
<style type="text/css">
  body > .wrapper {padding-top:0}
  body > .wrapper > .layer {display:none}

  .welcome::after {
    z-index: 1;
    background: #1E6E44 url("<?php the_field('imagem_full1', 'option'); ?>") no-repeat center/cover;
    opacity: .5;
  }
</style>
    <main class="wrapper">
      <header class="welcome is-dark has-after">
        <div class="wrapper is-centered has-spaces">
          <h1 class="title is-xlarge"><?php the_field('frase_de_destaque', 'option'); ?></h1>
          <p class="text"><?php the_field('introducao', 'option'); ?></p>
          <?php $btnsection1 = get_field('botao_1', 'option'); ?>
          <div class="wrapper"><?php foreach ($btnsection1 as $value) { ?><a class="button is-white" href="<?php echo $value['link']; ?>"><?php echo $value['texto']; ?></a><?php } ?></div>
        </div><span class="layer"></span>
      </header>
      <section class="featured">
        <div class="wrapper is-centered has-spaces">
          <header class="featured-header">
            <h2 class="title is-large"><?php the_field('titulo', 'option'); ?></h2>
            <?php $texto = get_field('texto_introducao', 'option'); ?>
            <p><?php echo nl2br( $texto );?></p>
            <a class="button" href="<?php the_field('link_botao', 'option'); ?>"><?php the_field('texto_botao', 'option'); ?></a>
          </header>
          <article class="featured-product is-dark">
            <h2 class="title is-large"><?php the_field('nome_do_produto', 'option'); ?></h2>
            <p class="text"><?php the_field('introducao_do_produto', 'option'); ?></p><a class="link is-upper has-after" href="<?php the_field('link', 'option'); ?>"><?php the_field('texto_do_link', 'option'); ?></a><img class="img" src="<?php the_field('imagem_do_produto', 'option'); ?>" alt="<?php the_field('nome_do_produto', 'option'); ?>">
          </article><span class="sprite is-seal"></span>
        </div>
      </section>
      <section class="location is-dark has-after">
        <div class="wrapper is-centered">
          <h2 class="title is-large"><?php the_field('titulo_sect3', 'option'); ?></h2>
          <?php $texto = get_field('texto_sect3', 'option'); ?>
          <p class="text"><?php echo nl2br( $texto );?></p>
          <a class="button is-white" href="<?php the_field('link_sect3', 'option'); ?>"><?php the_field('texto_do_botao_sect3', 'option'); ?></a>
        </div><span class="layer"></span>
      </section>
      <section class="contact">
        <div class="wrapper is-centered has-spaces">
          <header class="contact-header">
            <h2 class="title is-large"><?php the_field('titulo_da_secao_de_contato', 'option'); ?></h2>
            <p class="text"><?php the_field('texto_de_introducao_sect4', 'option'); ?></p>
          </header>
          <div class="contact-content">
            <div class="grid">
              <div class="grid-item">
                <h3 class="title is-xsmall is-upper">Telefones</h3>
                <p class="text"><?php the_field('telefone', 'option'); ?></p>
                <p class="text"><?php the_field('telefone_2', 'option'); ?></p>
              </div>
              <div class="grid-item">
                <h3 class="title is-xsmall is-upper">Onde Estamos</h3>
                <?php $texto = get_field('endereco', 'option'); ?>
                <p class="text"><?php echo nl2br( $texto );?></p>
              </div>
              <div class="grid-item">
                <h3 class="title is-xsmall is-upper">E-mail</h3>
                <p class="text"><?php the_field('e-mail', 'option'); ?></p>
              </div>
            </div><a class="button" href="<?php the_field('link_do_botao_sect4', 'option'); ?>"><?php the_field('texto_do_botao_sect4', 'option'); ?></a>
          </div>
          <div class="contact-buttons">
            <ul class="grid">
              <?php 
              $whatsapp = get_field('whatsapp', 'option');
              $countwhats = 0;
              foreach ($whatsapp as &$value) { $countwhats++; if ($countwhats<6){ ?>
              <li class="grid-item"><a class="button has-sprite" href="https://api.whatsapp.com/send?1=pt_BR&phone=<?php echo $value['num_whatsapp']; ?>" target="_blank"><?php echo $value['texto']; ?> <span class="sprite is-whats"></span></a></li>
              <?php } else { ?>
              <li class="grid-item"><a class="button has-sprite" href="<?php the_field('link_do_botao_sect4', 'option'); ?>">Ver mais cidades...<span class="sprite"></span></a></li>
              <?php break; } } ?>
            </ul>
          </div>
        </div><span class="layer"></span>
      </section>
      <section class="reviews is-dark">
        <div class="wrapper is-centered has-spaces">
          <h2 class="title is-large"><?php the_field('titulo_da_secao_de_depoimentos', 'option'); ?></h2>
          <p class="text"><?php the_field('texto_da_secao_de_depoimentos', 'option'); ?></p>
          <div class="swiper" data-name="reviews">
            <ul class="grid">
              <?php 
              $depoimentos = get_field('depoimento', 'option');
              $count = 0;
              $dialog = '';
              foreach ($depoimentos as &$value) { $count++; if ($count>9) break; ?>              
              <li class="grid-item">
                <article class="review is-light" data-popup data-target="#review_<?php echo $count; ?>">
                  <div class="wrapper"><img class="img is-cover" src="<?php echo $value['thumbnail']; ?>" alt=""><span class="sprite is-play"></span></div>
                  <h3 class="title is-xsmall"><?php echo $value['nome']; ?></h3>
                  <p class="text no-1"><?php echo $value['cidadeuf']; ?></p>
                  <p class="text no-2">"<?php echo $value['frase']; ?>"</p>
                </article>
              </li>           
            <?php 
                $dialog .= '<dialog class="popup" id="review_'.$count.'" style="display:none;"><figure class="wrapper"><img class="img is-cover" src="'.$value['imagem_do_video'].'" alt=""><span class="sprite is-play-2" onclick="player_video(\'#review_'.$count.'\', \''.$value['link_do_video'].'\');"></span></figure><h4 class="title is-xsmall">'.$value['nome'].'</h4><p class="text no-1">'.$value['cidadeuf'].'</p><p class="text no-2">"'.$value['frase'].'"</p><p class="text no-3">'.$value['data'].'</p><span class="sprite is-cancel" onclick="stopPlayer(\'#review_'.$count.'\', \''.$value['link_do_video'].'\', \''.$value['imagem_do_video'].'\')"></span></dialog>';
                //$dialog .= '<dialog class="popup" id="review_'.$count.'"><figure class="wrapper"><img class="img is-cover" src="media/lucas-hd.jpg" alt=""><span class="sprite is-play-2"></span></figure><h4 class="title is-xsmall">'.$value['nome'].'</h4><p class="text no-1">Cascavel/PR</p><p class="text no-2">"Lorem Ipsum is simply dummy text of the printing and typesetting industry. "Lorem Ipsum is simply dummy text of the printing and typesetting industry."Lorem Ipsum is simply dummy text of the printing.</p><p class="text no-3">27/03/2021</p><span class="sprite is-cancel"></span></dialog>';
            } ?>
            </ul>
          </div><a class="button is-white" href="<?php the_field('link_ver_mais', 'option'); ?>">Ver Mais</a>
        </div>
      </section>
      <section class="latest">
        <div class="wrapper is-centered has-spaces">
          <h2 class="title is-large">Notícias</h2>
          <p class="text">Acompanhe nossas principais informações!</p>
          <div class="swiper" data-name="latest">
            <ul class="grid">
              <?php
                $count = 0;
                if( have_posts() ) :

                while ( have_posts() ) :
                  $count++;
                  if ($count>9) break; 
                  the_post();
                  $postcat = get_the_category( $post->ID );     
              ?>              
              <li class="grid-item">
                <article class="card">
                  <figure class="wrapper"><?php the_post_thumbnail(array(320,223)); ?></figure>
                  <h3 class="title is-large"><?php the_title(); ?></h3>
                  <p class="text"><?php echo get_excerpt(100); ?></p><a class="link is-upper has-after" href="<?php the_permalink(); ?>">Leia mais +</a>
                </article>
              </li>
              <?php endwhile; endif; ?>
            </ul>
          </div><a class="button" href="/noticias">Ver Mais Notícias</a>
        </div>
      </section><span class="layer"></span>
    </main>
    <?php get_footer(); ?>
    <div class="overlay"></div>
    <?php
    echo $dialog;
    ?>
    <style>
      dialog.is-active {
        display: block !important;
      }
    </style>    

     <script>
       function player_video(idDialog, link){
          embed = '<iframe width="770" height="430" class="'+idDialog+'" src="https://www.youtube.com/embed/'+link+'?controls=0&autoplay=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';          
          elemento = idDialog + ' figure.wrapper';
          jQuery(elemento).html(embed);
       }
      
       function stopPlayer(idDialog, link, imagem){
          embed = '<img class="img is-cover" src="'+imagem+'" alt=""><span class="sprite is-play-2"  onclick="player_video(\''+idDialog+'\', \''+link+'\');"></span>';
          elemento = idDialog + ' figure.wrapper';
          jQuery(elemento).html(embed);          
       }
      
     </script>
  </body>
</html>