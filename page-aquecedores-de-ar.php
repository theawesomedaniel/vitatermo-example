<?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/products.css">
    <main class="wrapper">
      <section class="products is-dark">
        <div class="wrapper is-centered has-spaces">
          <h1 class="title is-medium">Categorias</h1>
          <?php 
            $args = array (
            'container' => 'ul',
            'menu_class' => 'grid', 
            'theme_location' => 'menu-categoria-produto',
            'walker' => new IBenic_Walker_category(),
            'depth' => 0
            );
            wp_nav_menu($args);
          ?> 
        </div>
      </section>
      <article class="category">
        <?php
          if( have_posts() ) : while ( have_posts() ) :
            the_post();
        ?>          
        <div class="wrapper is-centered has-spaces">
          <h1 class="title is-large"><?php the_title(); ?></h1>
          <h2 class="title is-medium">Características</h2>
          <p class="text"><?php the_field('texto_de_introducao'); ?></p>
          <ul class="grid">
            <?php 
            $caracteristicas = get_field('caracteristicas');
            foreach ($caracteristicas as &$value) { ?>  
            <li class="grid-item">
              <article class="category-item">
                <h3 class="title is-medium"><?php echo $value['titulo']; ?></h3>
                <p class="text"><?php echo $value['texto']; ?></p>
              </article>
            </li>
            <?php } ?>
          </ul>
          <?php $CategoryProdutos = get_field('modelos'); ?>
        <?php endwhile; endif ?>
       
          <h2 class="title is-medium">Modelos</h2>
          <ul class="grid">
          <?php 
              query_posts(array( 
                  'post_type' => 'produto',
                  'category_name' => $CategoryProdutos, 
                  'showposts' => 6, 
              ) );  
          ?>
          <?php while (have_posts()) : the_post(); ?>
          <?php 
          $thumb_id = get_post_thumbnail_id();
          $thumb_url = wp_get_attachment_image_src($thumb_id, 'thumbnail' , true);        
          ?>
            <li class="grid-item">
              <article class="category-model is-dark"><img class="img" src="<?php echo $thumb_url[0]; ?>" alt="">
                <h3 class="title is-medium product-title"><?php the_title(); ?></h3>
                <p class="text"><?php 
                $especificacoes = get_field('resumo_de_especificacoes');
                foreach ($especificacoes as &$value) { 
                  echo $value['rotulo'].": ". $value['valor']. "<br>";
                } ?></p><a class="link is-upper has-after" href="<?php the_permalink(); ?>">Ver Mais +</a>
              </article>
            </li>
          <?php endwhile; ?>
          </ul>
          <!--
          <div class="pages"><a class="sprite is-prev" href="noticias.html">Anterior</a>
            <ul class="grid is-auto">
              <li class="grid-item"><a class="link" href="noticias.html">1</a></li>
              <li class="grid-item"><a class="link" href="noticias.html">2</a></li>
              <li class="grid-item"><a class="link" href="noticias.html">3</a></li>
            </ul><a class="sprite is-next" href="noticias.html">Próximo</a>
          </div>
          -->

          <!-- PODE DAR PROBLEMA -->

          <div class="pages">
          <?php wordpress_pagination(); ?>
          </div>

          <!-- ENDPROBLEM -->

        </div>
      </article><span class="layer"></span>
    </main>
    <script src="assets/js/shuffle.js"></script>
    <?php get_footer(); ?>
  </body>
</html>