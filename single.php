<?php get_header(); ?>
    <main class="wrapper">
      <article class="article">
        <?php
            if ( have_posts() ) : 
                while  ( have_posts() ) : 
                    the_post();  


                    $thumb_url = wp_get_attachment_url( get_post_thumbnail_id() );
                    $thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');

        ?>
        <div class="wrapper is-centered has-spaces"><span class="text"><?php the_date('d/m/Y'); ?></span>
          <h1 class="title is-large"><?php the_title(); ?></h1>
          <img class="img" src="<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
          the_post_thumbnail_url();
          } else { echo '#'; }  ?>" alt="">
            <?php the_content(); ?>
        </div>
        
      <?php endwhile; endif; ?>
      </article><span class="layer"></span>
    </main>
  <?php get_footer(); ?>
  </body>
</html>