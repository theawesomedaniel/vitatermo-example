<?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/privacy.css">
    <style type="text/css">.footer-content{display:none}</style>
    <main class="wrapper">
      <section class="archive">
        <div class="wrapper is-centered has-spaces">
          <h1 class="title is-large">Notícias</h1>
          <ul class="grid is-shuffle">
          <?php
            $args = array_merge( array( 'post_status' => 'publish', 'order' => 'DESC', 'paged' => $paged, 'showposts' => 9 ) );
            query_posts( $args );

            while ( have_posts() ) : 
              the_post();
              $postcat = get_the_category( $post->ID );  ?>
            <li class="grid-item">
              <article class="card" style="margin-bottom: 20px;">
                <figure class="wrapper"><?php the_post_thumbnail(array(536,226)); ?></figure>
                <p class="title is-mini">
                <?php                      
                  foreach($postcat as $category){
                    echo $category->name;
                  }
                ?>
                </p>
                <h3 class="title is-large"><?php the_title(); ?></h3>
                <!-- <p class="text no-1"><?//php the_excerpt(150); ?></p> -->
                <p class="text no-1"><?php echo get_excerpt(100); ?></p>
                <p class="text no-2"><?php the_date('d/m/Y'); ?></p><a class="link is-upper has-after" href="<?php the_permalink(); ?>">Leia mais +</a>
              </article>
            </li>
            <?php endwhile; ?>
          </ul>
          <div class="pages">
          <?php wordpress_pagination(); ?>
          </div>

      </section><span class="layer"></span>
    </main>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/shuffle.js"></script>    
  <?php get_footer(); ?>
  </body>
</html>  
    