<?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/contact.css">
    <style type="text/css">
    .contact--seller > .sprite {
        position: absolute;
        top: calc(23% - 12px);
        right: 20px;
    }      
    </style>
    <main class="wrapper">
      <article class="contact">
        <?php
          if( have_posts() ) : while ( have_posts() ) :
            the_post();
        ?>          
        <div class="wrapper is-centered has-spaces">
          <header class="contact-header">
            <h1 class="title is-large">Formulário de Contato</h1>
            <?php
              echo do_shortcode('[contact-form-7 id="151" title="Contato" html_class="contact-h-form"]');
            ?>
          </header>
          <section class="contact-details">
            <ul class="grid">
              <li class="grid-item">
                <section class="contact--detail">
                  <h2 class="title is-xsmall is-upper">Telefones</h2>
                  <p class="text"><?php the_field('telefone', 'option'); ?></p>
                  <p class="text"><?php the_field('telefone_2', 'option'); ?></p>
                </section>
              </li>
              <li class="grid-item">
                <section class="contact--detail">
                  <h2 class="title is-xsmall is-upper">E-mail</h2>
                  <p class="text"><?php the_field('e-mail', 'option'); ?></p>
                </section>
              </li>
              <li class="grid-item">
                <section class="contact--detail">
                  <h2 class="title is-xsmall is-upper">Onde estamos</h2>
                  <?php $texto = get_field('endereco', 'option'); ?>
                  <p class="text"><?php echo nl2br( $texto );?></p>
                </section>
              </li>
            </ul>
          </section>
          <section class="contact-sellers">
            <h2 class="title is-large">Fale diretamente com um revendedor</h2>
            <ul class="grid">
              <?php 
              $whatsapp = get_field('whatsapp', 'option');
              foreach ($whatsapp as &$value) { ?>
              <li class="grid-item">
                <section class="contact--seller is-dark">
                  <h2 class="title is-xsmall is-upper"><?php echo $value['texto']; ?></h2>
                  <p class="text"><?php echo $value['nome']; ?> <br><?php echo $value['e-mail']; ?> <br><a href="https://api.whatsapp.com/send?1=pt_BR&phone=<?php echo $value['num_whatsapp']; ?>" target="_blank" style="color:inherit; text-decoration:none;"><?php echo $value['num_whatsapp']; ?></a></p><span class="sprite is-whats"></span>
                </section>
              </li>
              <?php } ?>
            </ul>
          </section>
        </div>
        <?php endwhile; endif; ?>
      </article><span class="layer"></span>
    </main>
    <?php get_footer(); ?>
    <script src="assets/js/mask.js"></script>
    <script src="assets/js/validate.js"></script>
    <div class="overlay"></div>
  </body>
</html>