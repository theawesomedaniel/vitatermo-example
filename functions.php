<?php

	function my_init_method() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'nav-menus' );
		add_theme_support( 'automatic-feed-links' ); 
			
		add_theme_support('menus');

		add_image_size('large', 2000, 1334, true);
		add_image_size('medium', 770, 430, true);
		add_image_size('thumbnail', 320, 223, true);

			
		register_nav_menus(
			array(
				'header-menu' => __( 'Menu Header', 'vitatermo'),
				'menu-footer' => __( 'Menu Footer', 'vitatermo'),
				'menu-mobile' => __( 'Menu Mobile', 'vitatermo'),
				'menu-categoria-produto' => __( 'Menu Categorias Produtos', 'vitatermo'),
			)
		);		

	}

	add_action('init', 'my_init_method');


	class IBenic_Walker extends Walker_Nav_Menu {

	    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
	    	$object = $item->object;
	    	$type = $item->type;
	    	$title = $item->title;
	    	$description = $item->description;
	    	$permalink = $item->url;
	      $output .= '<li class="grid-item '.  implode(" ", $item->classes) . '"><a class="link" href="' . $permalink . '">'.$title.'</a></li>';
	    }
	}

	class IBenic_Walker_category extends Walker_Nav_Menu {

	    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
	    	$object = $item->object;
	    	$type = $item->type;
	    	$title = $item->title;
	    	$description = $item->description;
	    	$permalink = $item->url;
	      $output .= '<li class="grid-item '.  implode(" ", $item->classes) . '"><a class="button is-white" href="' . $permalink . '">'.$title.'</a></li>';
	    }
	}

	// limitar o excerpt pela quantidade de caracteres - wpfoco
	function get_excerpt( $count ) {
		$permalink = get_permalink($post->ID);
		$excerpt = get_the_content();
		$excerpt = strip_tags($excerpt);
		$excerpt = substr($excerpt, 0, $count);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt = '<p>'.$excerpt.'... <a href="'.$permalink.'">Leia Mais</a></p>';
		return $excerpt;
	}

	if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'Theme General Settings',
			'menu_title'	=> 'Theme Settings',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));	

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Seção 1 - Banner destaque',
			'menu_title'	=> 'Seção 1 - Banner destaque',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Seção 2 - Produtos e Serviços',
			'menu_title'	=> 'Seção 2 - Produtos e Serviços',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Seção 3 - Sobre nós',
			'menu_title'	=> 'Seção 3 - Sobre nós',
			'parent_slug'	=> 'theme-general-settings',
		));		


		acf_add_options_sub_page(array(
			'page_title' 	=> 'Seção 4 Home - Contato',
			'menu_title'	=> 'Seção 4 Home - Contato',
			'parent_slug'	=> 'theme-general-settings',
		));			

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Depoimentos',
			'menu_title'	=> 'Depoimentos',
			'parent_slug'	=> 'theme-general-settings',
		));
	}


	//start postype produtos
	function post_type_produto() {
		register_post_type( 'produto',
		array(
		  'label'  => __( 'Produtos'),
	      'labels' => array(
	        'name' => __( 'Produto' ),
	        'singular_name' => __( 'Produto' ),
	        'add_new' => _x('Adicionar Novo', 'Novo Produto'),
	        'add_new_item' => __('Novo Produto'),
	        'edit_item' => __('Editar Produto'),
	        'new_item' => __('Novo Produto'),
	        'view_item' => __('Ver Produto'),
	        'search_items' => __('Procurar produto'),
	        'not_found' =>  __('Nenhum produto encontrado'),
	        'not_found_in_trash' => __('Nenhum produto encontrado na lixeira')
	      ),
	      	'public' => true,
	      	'menu_icon'  => 'dashicons-welcome-learn-more',
	      	'has_archive' => true,
	      	'supports' => array( 'title', 'thumbnail', 'excerpt' ),
	      	'rewrite' => true,
	      	'menu_position' => 4,	
	      	'taxonomies'  => array( 'category' ),
	    )
	  );
	}

	add_action( 'init', 'post_type_produto' );	

	// Function to change email address
	 
	function wpb_sender_email( $original_email_address ) {
	    return 'contato@vitatermo.com.br';
	    /* YI)?IP[%Q2Qu */
	}
	 
	// Function to change sender name
	function wpb_sender_name( $original_email_from ) {
	    return 'Contato Vitatermo';
	}
	 
	// Hooking up our functions to WordPress filters 
	add_filter( 'wp_mail_from', 'wpb_sender_email' );
	add_filter( 'wp_mail_from_name', 'wpb_sender_name' );	



	function wordpress_pagination() {
		global $wp_query;

		$big = 999999999;

		echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages
		) );
	}
