    <footer class="footer is-dark">
      <div class="wrapper is-centered"><a class="link has-sprite" href="inicio.html"><span class="sprite is-wlogo"></span> VitaTermo</a>
        <ul class="grid no-1 is-auto">
          <li class="grid-item"><a class="link has-sprite" href="<?php the_field('link_facebook', 'option'); ?>"><span class="sprite is-wface"></span> Facebook</a></li>
          <li class="grid-item"><a class="link has-sprite" href="<?php the_field('link_instagram', 'option'); ?>"><span class="sprite is-winsta"></span> Instragram</a></li>
        </ul>
        <?php 
          $args = array (
          'container' => 'ul',
          'menu_class' => 'grid no-2 is-auto', 
          'theme_location' => 'menu-footer',
          'walker' => new IBenic_Walker(),
          'depth' => 0
          );
          wp_nav_menu($args);
        ?><small class="text">Copyright © 2021 Vita Termo. Todos os direitos reservados. | Design e Desenvolvimento: <a href="https://www.agencia14bis.com.br/" style="color:inherit;">14BIS Agência de Marketing</a>.</small>
      </div>
    </footer>
    <dialog class="mobile is-dark" id="mobile" tab-index="-1">
      <?php 
        $args = array (
        'container' => 'ul',
        'menu_class' => 'grid', 
        'theme_location' => 'menu-mobile',
        'walker' => new IBenic_Walker(),
        'depth' => 0
        );
        wp_nav_menu($args);
      ?>       
      <button class="burger is-closed" id="mobile_close" type="button"><span class="text">Fechar</span><span class="layer"></span></button><span class="sprite is-seal"></span>
    </dialog>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/swiper.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/base.js?version=4"></script>
    <div class="overlay"></div>  
    <?php wp_footer(); ?>