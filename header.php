<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <title><?php
        // Returns the title based on the type of page being viewed
          if ( is_single() ) {
              single_post_title(); echo ' | '; bloginfo( 'name' );
          } elseif ( is_home() || is_front_page() ) {
              echo bloginfo( 'name' );     
          } elseif ( is_page() ) {
              single_post_title( '' ); echo ' | '; bloginfo( 'name' );
          } elseif ( is_search() ) {
              printf( __( 'Resultados de busca por "%s"', 'twentyten' ), esc_html( $s ) ); echo ' | '; bloginfo( 'name' );
          } elseif ( is_404() ) {
              echo ' | '; bloginfo( 'name' );
          } else {
              wp_title( '' ); echo ' | '; bloginfo( 'name' ); 
          }
      ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/base.css?version=2">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,400&amp;display=swap">
    <?php wp_head(); ?>
<style>

pages {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 40px;
}

.page-numbers {
  transition: background-color .25s;
}

.page-numbers:hover, .page-numbers:active {
  background-color: #F58634;
}

.page-numbers {
  overflow: hidden;
  border-radius: 50%;
  text-decoration: none;
}

.page-numbers {
  width: 32px;
  color: #1E6E44;
  font-weight: 600;
  line-height: 28px;
  text-align: center;
  border: 2px solid;
  border-radius: 50%;
  margin-left: 9px;
}  

.next{
  text-indent: -999px;
  background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprites.png) no-repeat 0 0;
  background-position: -512px 0;
  border: none;
  height: 32px;
}

.prev{
  text-indent: -999px;
  background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprites.png) no-repeat 0 0;
  background-position: -479px 0;
  border: none;
  height: 32px;
}

</style>    
  </head>
  <body>
    <header class="header has-shadow">
      <div class="wrapper is-centered"><a class="link has-sprite" href="https://vitatermo.com.br/novosite"><span class="sprite is-logo" style="image-rendering: -moz-crisp-edges; image-rendering: -o-crisp-edges; image-rendering: -webkit-optimize-contrast; image-rendering: crisp-edges;"></span> VitaTermo</a>
        <nav class="header-menu">
        <?php 
          $args = array (
          'container' => 'ul',
          'menu_class' => 'grid is-auto', 
          'theme_location' => 'header-menu',
          'walker' => new IBenic_Walker(),
          'depth' => 0
          );
          wp_nav_menu($args);
        ?> 
        </nav>
        <div class="header-info">
          <ul class="grid is-auto">
            <li class="grid-item"><span class="text"><?php the_field('telefone', 'option'); ?></span></li>
            <li class="grid-item"><span class="text"><?php the_field('e-mail', 'option'); ?></span></li>
            <li class="grid-item"><a class="link has-sprite" href="<?php the_field('link_facebook', 'option'); ?>"><span class="sprite is-face"></span> Facebook</a></li>
            <li class="grid-item"><a class="link has-sprite" href="<?php the_field('link_instagram', 'option'); ?>"><span class="sprite is-insta"></span> Instagram</a></li>
          </ul>
        </div>
        <button class="burger" id="mobile_open" type="button"><span class="text">Menu</span><span class="layer"></span></button>
      </div>
    </header>