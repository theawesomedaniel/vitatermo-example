<?php get_header(); ?>
    <main class="wrapper">
      <section class="archive">
        <div class="wrapper is-centered has-spaces">

          <?php
          if( have_posts() ) :

          while ( have_posts() ) :
              the_post(); ?>
          <!-- <div class="heading"> -->
            <!-- <p class="title is-mini">Depoimentos</p> -->
            <h1 class="title is-large"><?php the_title(); ?></h1>
            <?php the_content(); ?>

          <ul class="grid is-shuffle">

          <?php   
              $depoimentos = get_field('depoimento', 'option');
              $count = 0;
              foreach ($depoimentos as &$value) { $count++; ?>              
              <li class="grid-item">
                <article class="review is-light" data-popup data-target="#review_<?php echo $count; ?>">
                  <div class="wrapper"><img class="img is-cover" src="<?php echo $value['thumbnail']; ?>" alt=""><span class="sprite is-play"></span></div>
                  <h3 class="title is-xsmall"><?php echo $value['nome']; ?></h3>
                  <p class="text no-1"><?php echo $value['cidadeuf']; ?></p>
                  <p class="text no-2">"<?php echo $value['frase']; ?>"</p>
                </article>
              </li>
            <?php } ?>          
          </ul>
        <?php endwhile; endif; ?>

        <!-- inicia as funções de paginação aqui -->
        <!--
          <div class="pages"><a class="sprite is-prev" href="depoimentos.html">Anterior</a>
            <ul class="grid is-auto">
              <li class="grid-item"><a class="link" href="depoimentos.html">1</a></li>
              <li class="grid-item"><a class="link" href="depoimentos.html">2</a></li>
              <li class="grid-item"><a class="link" href="depoimentos.html">3</a></li>
            </ul><a class="sprite is-next" href="depoimentos.html">Próximo</a>
          </div>
        -->
        <!-- termina as funções de paginação aqui -->

        </div>  
      </section><span class="layer"></span>
    </main>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/shuffle.js"></script>
    <?php get_footer(); ?>

    <!-- <ul class="grid-item">  -->

    <?php 
    $depoimentos = get_field('depoimento', 'option');
    $count = 0;
    foreach ($depoimentos as &$value) { $count++; ?>        
    <dialog class="popup" id="review_<?php echo $count; ?>">
      <figure class="wrapper"><img class="img is-cover" src="<?php echo $value['imagem_do_video']; ?>" alt=""><span class="sprite is-play-2"  onclick="player_video('#review_<?php echo $count; ?>', '<?php echo $value['link_do_video']; ?>');"></span></figure>
      <h4 class="title is-xsmall"><?php echo $value['nome']; ?></h4>
      <p class="text no-1"><?php echo $value['cidadeuf']; ?></p>
      <p class="text no-2">"<?php echo $value['frase']; ?>"</p>
      <p class="text no-3"><?php echo $value['data']; ?></p><span class="sprite is-cancel" onclick="stopPlayer('#review_<?php echo $count; ?>', '<?php echo $value['link_do_video']; ?>');"></span>
    </dialog>
     <?php } ?>


     <script>
       function player_video(idDialog, link){
          embed = '<iframe width="770" height="430" class="'+idDialog+'" src="https://www.youtube.com/embed/'+link+'?controls=0&autoplay=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';          
          elemento = idDialog + ' figure.wrapper';
          jQuery(elemento).html(embed);
       }
      
       function stopPlayer(idDialog, link){
          embed = '<img class="img is-cover" src="<?php echo $value['imagem_do_video']; ?>" alt=""><span class="sprite is-play-2"  onclick="player_video(\''+idDialog+'\', \''+link+'\');"></span>';
          elemento = idDialog + ' figure.wrapper';
          jQuery(elemento).html(embed);          
       }
      
     </script>
  </body>
</html>