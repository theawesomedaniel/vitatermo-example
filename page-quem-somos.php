<?php get_header(); ?>
    <main class="wrapper">
      <article class="wrapper">
        <?php
          if( have_posts() ) : while ( have_posts() ) :
            the_post();
        ?>                          
        <header class="location is-dark is-header has-after">
          <div class="wrapper is-centered">
            <h2 class="title is-large"><?php the_title(); ?></h2>
            <?php the_content(); ?>
          </div>
        </header>
        <div class="values">
          <div class="wrapper is-centered has-spaces">
            <ul class="grid">
              <li class="grid-item">
                <article class="values-item is-dark">
                  <h2 class="title is-xsmall is-upper">MISSÃO</h2>
                  <p class="text"><?php the_field('missao'); ?></p><span class="sprite is-mission"></span>
                </article>
              </li>
              <li class="grid-item">
                <article class="values-item is-dark">
                  <h2 class="title is-xsmall is-upper">VISÃO</h2>
                  <p class="text"><?php the_field('visao'); ?></p><span class="sprite is-vision"></span>
                </article>
              </li>
              <li class="grid-item">
                <article class="values-item is-dark">
                  <h2 class="title is-xsmall is-upper">VALORES</h2>
                  <p class="text"><?php the_field('valores'); ?></p><span class="sprite is-values"></span>
                </article>
              </li>
            </ul>
          </div>
        </div>
        <?php endwhile; endif; ?>        
      </article><span class="layer"></span>
    </main>
    <?php get_footer(); ?>
  </body>
</html>