<?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/doubts.css">
    <main class="wrapper">
      <div class="wrapper is-centered has-spaces">
        <article class="doubts">
          <header class="doubts-header">
            <h1 class="title is-medium">Dúvidas frequentes</h1>
            <p class="text">Se ainda assim possuir alguma dúvida, você pode entrar em contato e falar conosco.</p><a class="button is-yellow" href="contato.html">Entrar em contato</a>
          </header>
          <div class="doubts-content has-shadow">
            <ul class="grid">
              <li class="grid-item">
                
                <div class="doubts-content-dropdown">
                  <div class="wrapper">
<!-- 					  PROBLEMA PODE ESTAR AQUI -->
					  
			  $duvidas = get_field('duvida', 'option');
              $count = 0;
              foreach ($duvida as &$value) { $count++; ?>              
              <h2 class="title is-xsmall has-sprite">Ainda não recebi meu pedido, o que fazer? <span class="sprite is-black-down-arrow"></span></h2>
                  <h3 class="title is-xsmall"><?php echo $value['nome']; ?></h3>
                  <p class="text no-1"><?php echo $value['cidadeuf']; ?></p>
                  <p class="text no-2">"<?php echo $value['frase']; ?>"</p>
                </article>
              </li>
            <?php } ?>  
				  
<!-- 				  FIM DO POSSÍVEL PROBLEMA -->
				  
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                  </div>
                </div>
              </li>
              
              <li class="grid-item">
                <h2 class="title is-xsmall has-sprite">Ainda não recebi meu pedido, o que fazer? <span class="sprite is-black-down-arrow"></span></h2>
                <div class="doubts-content-dropdown">
                  <div class="wrapper">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                  </div>
                </div>
              </li>
              <li class="grid-item">
                <h2 class="title is-xsmall has-sprite">Ainda não recebi meu pedido, o que fazer? <span class="sprite is-black-down-arrow"></span></h2>
                <div class="doubts-content-dropdown">
                  <div class="wrapper">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </article>
      </div>
    </main>
<?php get_footer(); ?>