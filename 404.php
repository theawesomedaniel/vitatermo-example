<?php get_header(); ?>
    <main class="wrapper">
      <section class="archive">
        <div class="wrapper is-centered has-spaces">
          <ul class="grid is-shuffle">
            <li class="grid-item">
              <article class="card" style="margin-bottom: 20px;">
                <h3 class="title is-large">Não encontramos o que procurava!</h3>
                <!-- <p class="text no-1"><?//php the_excerpt(150); ?></p> -->
                <p class="text no-1"></p>
              </article>
            </li>
          </ul>
      </section><span class="layer"></span>
    </main>
  <?php get_footer(); ?>
  </body>
</html>    