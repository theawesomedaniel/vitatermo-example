<?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/products.css?version=3">    
    <style>
    .is-dark .field{
      width: 260px;
      height: 60px;
    }
    </style>
    <main class="wrapper">
      <section class="products is-dark">
        <div class="wrapper is-centered has-spaces">
          <h1 class="title is-medium">Categorias</h1>
          <?php 
            $args = array (
            'container' => 'ul',
            'menu_class' => 'grid', 
            'theme_location' => 'menu-categoria-produto',
            'walker' => new IBenic_Walker_category(),
            'depth' => 0
            );
            wp_nav_menu($args);
          ?>           
        </div>
      </section>
      <article class="product">
        <?php
          if( have_posts() ) : while ( have_posts() ) :
            the_post();
        ?>                  
        <div class="wrapper is-centered has-spaces">
          <figure class="product-images">
            <div class="swiper" data-name="product">
              <ul class="grid">
                <?php 
                $thumb_id = get_post_thumbnail_id();
                $thumb_url = wp_get_attachment_image_src($thumb_id, 'medium' , true);        
                ?>                
                <li class="grid-item"><a class="link" href="<?php echo $thumb_url[0]; ?>"><img class="img" src="<?php echo $thumb_url[0]; ?>" alt="Foto 1"></a></li>
                <?php $galeria = get_field('galeria'); $count = 1;
                foreach ($galeria as $value){ $count++; ?>
                <li class="grid-item"><a class="link" href="<?php echo $value; ?>"><img class="img" src="<?php echo $value; ?>" alt="Foto <?php echo $count; ?>"></a></li>
                <?php } ?>
              </ul>
            </div>
          </figure>
          <div class="product-content">
            <h1 class="title is-medium"><?php the_title(); ?></h1>
            <p class="title is-xsmall">Especificações</p>
            <p class="text"><?php 
            $especificacoes = get_field('especificacoes');
            foreach ($especificacoes as &$value) { 
            echo $value['rotulo'].": ". $value['valor']. "<br>";
            } ?></p>
            <?php
              echo do_shortcode('[contact-form-7 id="153" title="solicitacao de orcamento" html_class="product-c-contact is-dark"]');
            ?>
            <!--
            <div id="wpcf7-f153-p100-o1">
            <form class="product-c-contact is-dark" action="post">
              <p class="title is-xsmall">Preencha seus dados e solicite o contato!</p>
              <ul class="grid">
                <li class="grid-item">
                  <input class="field" name="name" type="text" placeholder="Nome" required>
                </li>
                <li class="grid-item">
                  <input class="field" name="phone" type="tel" placeholder="Telefone" required>
                </li>
                <li class="grid-item">
                  <input class="field" name="email" type="email" placeholder="E-mail" required>
                </li>
                <li class="grid-item">
                  <input type="submit" value="Solicitar orçamento" class="wpcf7-form-control wpcf7-submit button">
                </li>
              </ul>
              <p>
              <button class="button" type="button">Solicitar orçamento</button>
              </p>
              <span class="text">Seus dados foram enviados com sucesso, em breve entraremos em contato!</span>
            </form>
            </div>
            -->

          </div>
          <div class="product-sections">
            <h2 class="title is-medium is-upper">Dados Técnicos</h2>
            <ul class="grid">
              <li class="grid-item">
                <section class="product--section">
                  <h3 class="title is-medium">Construtivos</h3>
                  <?php $texto = get_field('dados_tecnico_construtivos'); ?>
                  <p class="text"><?php echo nl2br( $texto ); ?></p>
                </section>
              </li>
              <li class="grid-item">
                <section class="product--section">
                  <h3 class="title is-medium">Operacionais</h3>
                  <?php $texto = get_field('dados_tecnicos_operacionais'); ?>
                  <p class="text"><?php echo nl2br( $texto ); ?></p>
                </section>
              </li>
            </ul>
          </div>
        </div>
      <?php endwhile; endif; ?>
      </article><span class="layer"></span>
    </main>
    <?php get_footer(); ?>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/validate.js"></script>    
  </body>
</html>