<?php get_header(); ?>
    <main class="wrapper">
      <section class="archive">
        <div class="wrapper is-centered has-spaces">
          <ul class="grid is-shuffle">
          <?php
            if( have_posts() ) :

            while ( have_posts() ) :
              the_post();
              $postcat = get_the_category( $post->ID );     
          ?>
            <li class="grid-item">
              <article class="card" style="margin-bottom: 20px;">
                <figure class="wrapper"><?php the_post_thumbnail(array(536,226)); ?></figure>
                <p class="title is-mini">
                <?php                      
                  foreach($postcat as $category){
                    echo $category->name;
                  }
                ?>
                </p>
                <h3 class="title is-large"><?php the_title(); ?></h3>
                <!-- <p class="text no-1"><?//php the_excerpt(150); ?></p> -->
                <p class="text no-1"><?php echo get_excerpt(100); ?></p>
                <p class="text no-2"><?php the_date('d/m/Y'); ?></p><a class="link is-upper has-after" href="<?php the_permalink(); ?>">Leia mais +</a>
              </article>
            </li>
            <?php endwhile; endif; ?>
          </ul>

                <!-- <li><?php wordpress_pagination(); ?></li> -->

        <!--   <div class="pages">
          <a class="sprite is-prev" href="<?php previous_pages_link(); ?>">Anterior</a>
            <ul class="grid is-auto">
              <li class="grid-item"><a class="link" href="index.php">1</a></li>
              <li class="grid-item"><a class="link" href="index.php">2</a></li>
              <li class="grid-item"><a class="link" href="index.php">3</a></li>
            </ul><a class="sprite is-next" href="<?php the_permalink(); ?>">Próximo</a>
          </div>
        </div>>  -->

      </section><span class="layer"></span>
    </main>
  <?php get_footer(); ?>
  </body>
</html>    