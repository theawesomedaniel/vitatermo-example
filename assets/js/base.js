$(function(){
  
  var $window = $(window);
  var winWidth;
  var winHeight;
  var winOffset;
  var $body = $('body');
  
  !function(){
    
    var $header = $('.header');
    var headerOffset = 0;
    
    $window.on({
      resize: function(){
        winOnResize();
      },
      scroll: function(){
        winOnScroll();
      },
      load: function(){
        winOnResize();
        winOnScroll();
      }
    });
    
    function winOnResize() {
      winWidth = $window.innerWidth();
    }
    
    function winOnScroll() {
      winOffset = $window.scrollTop();
      if (winOffset) {
        if (winOffset > headerOffset) {
          $header.addClass('is-scroll');
        } else {
          $header.removeClass('is-scroll');
        }
      } else {
        $header.removeClass('is-scroll');
      }
      headerOffset = winOffset;
    }
    
  }();
  
  $('#mobile').each(function(){
    
    var $mobile = $(this);
    var $mobile_open = $('#mobile_open');
    var $mobile_close = $('#mobile_close');
    
    $mobile_open.on('click', function(){
      $body.addClass('is-mobile');
    });
    
    $mobile_close.on('click', function(){
      $body.removeClass('is-mobile');
    });
    
    $mobile.find('.link.is-anchor').on('click', function(){
      $body.removeClass('is-mobile');
    });
    
  });
  
  $('.swiper').each(function(){
    
    var $this = $(this);
    var name = $this.data('name');
    var args = null;
    var swiper;
    var swiper2;
    var $target;
    
    switch (name) {
      
      case 'reviews': case 'latest':
        
        $('<button class="sprite is-left is-' + (name === 'reviews' ? 'wprev' : 'prev') + '" type="button">Anterior</button>').appendTo($this);
        $('<button class="sprite is-right is-' + (name === 'reviews' ? 'wnext' : 'next') + '" type="button">Próximo</button>').appendTo($this);
        $('<div class="swiper-bullets"></div>').appendTo($this);
        
        args = {
          breakpoints: {
            600: {
              slidesPerView: 2
            },
            1000: {
              slidesPerView: 3
            }
          },
          pagination: {
            el: '.swiper-bullets',
            clickable: true
          },
          navigation: {
            prevEl: '.sprite.is-left',
            nextEl: '.sprite.is-right'
          },
          watchSlidesVisibility: true
        };
        
      break; case 'product':
        
        var $target = $this.clone();
        
        $('<button class="sprite is-left is-prev" type="button">Anterior</button>').appendTo($target);
        $('<button class="sprite is-right is-next" type="button">Próximo</button>').appendTo($target);
        
        $target.removeAttr('data-name').appendTo($this.parent());
        
        swiper2 = new Swiper($target[0], {
          slidesPerView: 2,
          breakpoints: {
            400: {
              slidesPerView: 3
            },
            600: {
              slidesPerView: 4
            },
            800: {
              slidesPerView: 5
            }
          },
          navigation: {
            prevEl: '.sprite.is-left',
            nextEl: '.sprite.is-right'
          },
          watchSlidesVisibility: true
        });
        
        args = {
          thumbs: {
            swiper: swiper2
          },
          effect: 'fade',
          fadeEffect: {
            crossFade: true
          }
        };
        
      break; case 'privacy':
        
        args = {
          effect: 'fade',
          fadeEffect: {
            crossFade: true
          },
          autoHeight: true
        };
        
      break; default:
        
        args = {
          
        };
        
    }
    
    swiper = new Swiper(this, args);
    
    var $links;
    
    if (name === 'product') {
      
      $links = $target.find('.link');
      
      $links.on('click', function(ev){
        
        ev.preventDefault();
        
        var index = $(this).parent().index();
        
        swiper.slideTo(index);
        swiper2.slideTo(index);
        
        $links.removeClass('is-active').eq(index).addClass('is-active');
        
      }).eq(0).addClass('is-active');
      
      swiper.on('slideChange', function(){
        
        swiper2.slideTo(this.activeIndex);
        
        $links.removeClass('is-active').eq(this.activeIndex).addClass('is-active');
        
      });
      
    }
    
  });
  
  $('.grid.is-shuffle').each(function(){
    
    new Shuffle(this, {
      itemSelector: '.grid-item'
    })
    
  });
  
  $('#wpcf7-f153-p219-o1 .product-c-contact').each(function(){
    
    var $contact = $(this);
    var $button = $contact.find('.btnOrcamento');
    
    $button.on('click', function(){
      $contact.addClass('is-open');
    });
    
    /*
    $contact.validate({
      messages: {
        name: 'Por favor, insira seu nome',
        phone: 'Por favor, insira seu telefone',
        email: {
          required: 'Por favor, insira seu email',
          email: 'O email é inválido'
        }
      },
      submitHandler: function(form) {
        form.reset();
        $contact.addClass('is-send');
        setTimeout(function(){
          setTimeout(function(){
            $contact.removeClass('is-open is-send');
          }, 3000);
        }, 1000);
      }
    });
    */
    
  });
  
  $('.contact-h-form').each(function(){
    
    var $contact = $(this);
    
    $contact.validate({
      messages: {
        name: 'Por favor, insira seu nome',
        phone: 'Por favor, insira seu telefone',
        email: {
          required: 'Por favor, insira seu email',
          email: 'O email é inválido'
        },
        message: 'Por favor, insira a mensagem'
      },
      submitHandler: function(form) {
        $contact.addClass('is-send');
        setTimeout(function(){
          form.reset();
          setTimeout(function(){
            $contact.removeClass('is-send');
          }, 3000);
        }, 500);
      }
    });
    
    $('#phone').mask('(00) 00000-0000');
    
  });
  
  
  !function(){
    
    var $triggers = $('[data-popup]');
    var $popups = $('.popup');
    var $overlay = $('.overlay');
    
    if (!$triggers.length || !$popups.length) {
      return;
    }
    
    $triggers.on('click', function(ev){
      ev.preventDefault();
      var $trigger = $(this);
      var $popup = $($trigger.attr('data-target'));
      if ($popup.length) {
        $body.addClass('is-popup');
        setTimeout(function(){
          $popup.addClass('is-active');
        }, 99);
      }
    });
    
    $overlay.on('click', function(){
      $popups.removeClass('is-active');
      setTimeout(function(){
        $body.removeClass('is-popup');
      }, 99);
    });
    
    $popups.find('.sprite.is-cancel').on('click', function(){
      $popups.removeClass('is-active');
      setTimeout(function(){
        $body.removeClass('is-popup');
      }, 99);
    });
    
  }();
  
  
  !function(){
    
    var $html = $('html, body');
    var $header = $('.header');
    
    $('.link.is-anchor').on('click', function(ev){
      
      ev.preventDefault();
      
      var $target = $(this.hash);
      
      if ($target.length) {
        
        $html.animate({
          scrollTop: $target.offset().top - (winWidth >= 800 ? $header.height() : 0)
        }, 500);
        
      }
      
    });
    
  }();
  
});