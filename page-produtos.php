<?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/products.css">
    <main class="wrapper">
      <section class="products is-dark">
        <div class="wrapper is-centered has-spaces">
          <h1 class="title is-large">Conheça nossos produtos</h1>
          <p class="text">Selecione uma categoria abaixo e veja os produtos que temos para você! </p>
          <?php 
            $args = array (
            'container' => 'ul',
            'menu_class' => 'grid', 
            'theme_location' => 'menu-categoria-produto',
            'walker' => new IBenic_Walker_category(),
            'depth' => 0
            );
            wp_nav_menu($args);
          ?> 
        </div>
      </section>
      <section class="services">
        <?php
          if( have_posts() ) : while ( have_posts() ) :
            the_post();
        ?>                  
        <div class="wrapper is-centered has-spaces">
          <h2 class="title is-large"><?php the_title(); ?></h2>
          <ul class="grid is-shuffle">
            
            <?php 
            $itens = get_field('itens');
            foreach ($itens as &$value) { ?>
            <li class="grid-item">  
              <article class="services-item">
                <h3 class="title is-medium"><?php echo $value['titulo']; ?></h3>
                <p class="text"><?php echo $value['texto']; ?></p><img class="img is-cover" src="<?php echo $value['imagem']; ?>" alt="">
              </article>
            </li>
          <?php } ?>
          </ul>
        </div>
        <?php endwhile; endif; ?>
      </section><span class="layer"></span>
    </main>
    <?php get_footer(); ?>
  </body>
</html>